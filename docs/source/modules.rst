Modules
=======

biorobotics
-----------

Tickers
~~~~~~~

.. autoclass:: biorobotics.Ticker
    :members:
    :undoc-members:



Tic / Toc
~~~~~~~~~

.. autofunction:: biorobotics.tic
.. autofunction:: biorobotics.toc



SerialPC
~~~~~~~~

.. autoclass:: biorobotics.SerialPC
    :members:
    :undoc-members:



AnlogIn
~~~~~~~

.. autoclass:: biorobotics.AnalogIn
    :members:
    :undoc-members:



PWM
~~~

.. autoclass:: biorobotics.PWM
    :members:
    :undoc-members:



Encoder
~~~~~~~

.. autoclass:: biorobotics.Encoder
    :members:
    :undoc-members:



pyb
---

Timer
~~~~~

.. autoclass:: pyb.Timer
    :members:



LED
~~~

.. autoclass:: pyb.LED
    :members:



Switch
~~~~~~

.. autoclass:: pyb.Switch
    :members:



Pin
~~~

.. autoclass:: pyb.Pin
    :members:



UART
~~~~

.. autoclass:: pyb.UART
    :members:


Functions
~~~~~~~~~

.. autofunction:: pyb.elapsed_millis
.. autofunction:: pyb.millis
.. autofunction:: pyb.delay
.. autofunction:: pyb.micros
.. autofunction:: pyb.udelay



stm
---

.. automodule:: stm

.. data:: stm.USART_CR1
.. data:: stm.USART3



uart3br
-------

.. autofunction:: uart3br.send



utime
-----

Functions
~~~~~~~~~

.. autofunction:: utime.ticks_ms
.. autofunction:: utime.ticks_us
.. autofunction:: utime.test_set_ticks


Module Info
-----------

.. automodule:: biorobotics
    :members:
    :undoc-members:
    :show-inheritance: