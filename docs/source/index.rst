.. Micropython Dummy Modules documentation master file, created by
   sphinx-quickstart on Fri Jan  8 15:34:40 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Micropython Dummy Modules documentation!
===================================================================

The Micropython Dummy Modules is a package that mirrors the modules of micropython, such that a micropython program can be run on a computer to test and debug it.

This is made for the BioRobotics fork of micropython, see the wiki: https://bitbucket.org/ctw-bw/micropython/wiki/Home

Also note the official micropython documentation: http://docs.micropython.org/en/latest/library/index.html

The `biorobotics` package is included in these dummy modules directly. So the reference here for that module is also appropriate for development on the microcontroller.
The other modules **are mocked**, and although their interface should be identical to the real thing, it is better to use the real reference when using them on the microcontroller.

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   installation
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
