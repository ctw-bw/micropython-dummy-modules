# Micropython Dummy Modules

[![CodeFactor](https://www.codefactor.io/repository/bitbucket/ctw-bw/micropython-dummy-modules/badge)](https://www.codefactor.io/repository/bitbucket/ctw-bw/micropython-dummy-modules)
[![codecov](https://codecov.io/bb/ctw-bw/micropython-dummy-modules/branch/master/graph/badge.svg)](https://codecov.io/bb/ctw-bw/micropython-dummy-modules)
[![Documentation Status](https://readthedocs.org/projects/micropython-dummy-modules-biorobotics/badge/?version=latest)](https://micropython-dummy-modules-biorobotics.readthedocs.io/en/latest/?badge=latest)

The file in this repository contain packages which are meant as an alternative to real micropython modules, to be run on a PC without actual hardware interfacing. The goal is to allow MCU scripts to be run locally without any necessary changes.

All input/output operation is missing, however, the interfaces are intended to be the same. `pyb.LED` for instance will not light up an LED (there would be none available on a Windows laptop). However, the state is tracked such that code can remain the same.  
In addition to this, some of the dummy classes have a method `test_set_value()` with which you can change the underlying values to mimic e.g. a button press from your computer command line.

The latest documentation can be found at: **https://micropython-dummy-modules-biorobotics.readthedocs.io/en/latest/**

## Install

There are several methods to make the dummy modules available for your project. Below is the recommended approach. See the other methods and more information in [INSTALL.md](INSTALL.md)

### Pip

To install the modules in your regular Python installation (basic):

 1. Clone or download this repository.
    * You will also need to get the source of the `biorobotics` sub-package, so clone with the `--recursive` flag. If you already cloned, run `git submodule update --init` to acquire the dependencies.
 3. Open a command line and run `pip install path/to/dummy/modules`
    *  You can also open a command line inside the cloned directory and run `pip install .`
 4. The micropython packages should now be available everywhere.

If you are using Python for other things than micropython it is recommended to install the micropython packages in a virtual environment (advanced):

 1. Clone or download this repository.
 2. Create a virtual environment in your current directory: `python -m venv venv`
 3. Activate it by running `venv/scripts/activate.bat` (use `Activate.ps1` for PowerShell)
 4. Run `pip install path/to/dummy/modules`
    *  The latest version of `biorobotics-BE` will be installed automatically. To install a different version, run a different `pip install` for it _after_ having installed the dummy modules package. 
 6. The micropython packages are now available in this venv.

### Uninstall

To uninstall the modules (global install or venv), run:

```
pip uninstall micropython-dummy-modules-BE
```

(Don't forget the "-BE" at the end)

## Dummy Features

Most methods and features will be empty stubs. There are also a few methods that exist in the dummy objects that the original ones do not have. These can be used for example from the command line to simulate a button press or another input. The additional methods are:

 * `Switch.test_set_value(bool)`: Set Switch value to either `True` or `False` (callbacks are triggered if set)
 * `Switch.test_push(bool=True)`: Set Switch to a new value (`True` by default), trigger callbacks if any, wait a moment and reset the state again. This is a short notation to simulate a button push and release
 * `Pin.test_set_value(value)`: Set Pin value, 0 or 1 (should only be relevant for input pins) (callbacks are triggered if set according to trigger modes)
 * `Pin.test_push(value=1)`: Set Pin to new value (1 by default), trigger callbacks if any, wait a moment and reset the state again. This is to simulate a measured pulse
 * `ADC.test_set_value(value)`: Write ADC value (0 to 65535)
 * `LED.test_is_on()`: Check if LED would be on

To call dummy methods on objects of `biorobotics`, use the underlying object:

 * `AnalogIn.adc.test_set_value()`: Write a value to the AnalogIn object
 * `PWM.channel.test_get_value()`: Read the current pwm (as percentage) from the PWM object

### Example

Your microcontroller program could be:

```
# main.py
from pyb import Switch


def hello():
    print('Hello World!')


sw = Switch()
sw.callback(hello)
```

This will run normally on your Nucleo. If you run this on your PC, nothing will happen at first. But if we interact with the switch object:

```
$ python -i main.py
>> 
>> sw.test_set_value(True)
Hello World!
>> 
```

Note that, like on the real Nucleo, different objects pointing to the same hardware share the same values:

```
>> import pyb
>> pyb.Switch().test_set_value(True)
Hello World!
>> 
```

### Ticker

Note that the ticker object (`biorobotics.Ticker`) will not keep your script alive. If your main script creates and starts a ticker, it will return immediately afterwards. To keep your ticker running, run your script in interactive mode:

```
python -i main.py
```

Because of interactive mode the ticker will keep going. Stop it like you would normally exit a Python console: by hitting [Ctrl] + [Z] followed by [Enter], typing `exit()` or killing the terminal.

When using a ticker you can interact with your code on your PC just like you would through the REPL on micropython.

## Developing

See [CONTRIBUTING.md](CONTRIBUTING.md)
