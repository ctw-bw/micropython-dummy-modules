"""
Import this file to verify these dummy modules are not loaded in
micropython

This is probably not necessary, because even when these files are uploaded
to the microcontroller, the system pyb and machine packages are still
preferred.

`sys.platform` returns "win32" or "win64" on Windows and "linux" on Linux.
"""

import sys

if sys.platform in ['pyboard', 'micropython', 'stm']:
    raise RuntimeError('The dummy modules are being imported on the real '
                       'microcontroller! Remove these modules from the '
                       'microcontroller, they should only be used on PC')
