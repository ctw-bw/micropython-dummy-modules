import unittest
from unittest import mock

from biorobotics import SerialPC


class TestSerialPC(unittest.TestCase):
    """Test SerialPC class

    Patch a mock object to the actual send function, so we can capture what
    had happened.
    """

    @mock.patch('uart3br.send')
    def test_send(self, send_mock: mock.MagicMock):
        pc = SerialPC(2)

        pc.set(0, 3.14)
        pc.set(1, 420.0)
        pc.send()

        calls = send_mock.call_count
        self.assertEqual(4, calls)  # Channels, time and 2 floats (4 calls)

    @mock.patch('uart3br.send')
    def test_send_list(self, send_mock: mock.MagicMock):

        pc = SerialPC(2)

        pc.set_list([3.14, 420])
        pc.send()

        calls = send_mock.call_count
        self.assertEqual(4, calls)  # Channels, time and 2 floats (4 calls)

    def test_send_list_incorrect_size(self):
        pc = SerialPC(2)
        with self.assertRaises(ValueError):
            pc.set_list([3.14, 420.0, 0.69])


if __name__ == '__main__':
    unittest.main()
