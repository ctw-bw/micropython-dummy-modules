import unittest
import sys
from unittest import mock


class TestSystemCheck(unittest.TestCase):
    """Test the pyb.systemcheck

    This is its own module since pyb cannot already have been imported.
    """

    def tearDown(self):
        """Clean up after each tests"""
        for key in ['pyb', 'pyb._pyb', 'pyb._system_check']:
            if key in sys.modules:
                del sys.modules[key]

    def test_check(self):
        import pyb
        pyb.Switch()

    @mock.patch('sys.platform', 'pyboard')
    def test_check_fail(self):
        with self.assertRaises(RuntimeError):
            import pyb
            pyb.Switch()


if __name__ == '__main__':
    unittest.main()
