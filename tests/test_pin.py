import unittest
from unittest import mock

from pyb import Pin


class TestPin(unittest.TestCase):

    def test_pin(self):
        p = Pin('D5', mode=Pin.IN, pull=Pin.PULL_NONE)
        self.assertEqual(0, p.value())
        Pin('D5').test_set_value(1)
        self.assertEqual(1, p.value())

    def test_callback(self):

        callback = mock.MagicMock()

        p = Pin('D5', mode=Pin.IN, pull=Pin.PULL_NONE)
        p.irq(callback, trigger=Pin.IRQ_RISING | Pin.IRQ_FALLING)

        p.test_set_value(1)
        callback.assert_called_once()
        p.test_set_value(0)
        self.assertEqual(2, callback.call_count)

    def test_push(self):

        callback = mock.MagicMock()

        p = Pin('D5', mode=Pin.IN, pull=Pin.PULL_NONE)
        p.test_set_value(0)
        p.irq(callback, trigger=Pin.IRQ_RISING)

        p.test_push()
        callback.assert_called_once()
        self.assertEqual(0, p.value())


if __name__ == '__main__':
    unittest.main()
