import unittest
from unittest import mock
import time

from pyb import Timer, delay, udelay, millis, micros, elapsed_millis
from utime import ticks_us, test_set_ticks


class MyClass:
    """Simple class used in tests"""

    count = 0

    def __init__(self):
        self.value = 0

    def func(self, t):
        self.value += 1


class TestTimer(unittest.TestCase):

    def test_timer(self):
        """Set simple function callback"""

        callback = mock.MagicMock()

        t = Timer(5, freq=100.0, callback=callback)  # Starts by default

        time.sleep(0.20)  # Let timer run a bit (callback should fire 20
        # times)

        t.deinit()  # Disable Timer

        time.sleep(0.30)  # Let timer run a bit

        count = callback.call_count

        # Accept around 20 calls, not exact because time.sleep() and the
        # threading timer are both not accurate
        self.assertTrue(10 <= count <= 30, 'Function was called {} '
                                           'times'.format(count))

        time.sleep(0.30)  # Let timer run a bit

        self.assertEqual(count, callback.call_count)
        # Make sure it hasn't increased, i.e. the callback really stopped

    def test_method_callback(self):
        """Test the time callback when it points to a class method"""

        obj = MyClass()

        t = Timer(5, freq=100.0, callback=obj.func)  # Starts by default

        time.sleep(0.10)

        self.assertGreaterEqual(obj.value, 4)

    def test_non_blocking(self):
        """Make sure the timer object itself does not block the interpreter"""

        callback = mock.MagicMock()

        t_start = time.time()

        t = Timer(5, freq=100.0, callback=callback)  # Starts by default

        time_taken = time.time() - t_start

        self.assertLessEqual(time_taken, 0.05)

    def test_workspace(self):
        """Make sure the timers use the same variables workspace"""

        MyClass.count = 0

        def my_func(_t):
            MyClass.count += 1

        t = Timer(5, freq=100.0, callback=my_func)  # Starts by default

        time.sleep(0.20)

        self.assertGreater(MyClass.count, 0)  # This workspace has been
        # modified


class TestFunctions(unittest.TestCase):

    def setUp(self) -> None:
        test_set_ticks(0.0)  # Reset virtual runtime to zero

    def test_delay(self):
        t_start = time.time()
        delay(500)
        time_taken = time.time() - t_start
        self.assertGreaterEqual(time_taken, 0.4)
        self.assertGreaterEqual(millis(), 400)
        self.assertGreaterEqual(elapsed_millis(), 400)

    def test_udelay(self):
        t_start = time.time()
        udelay(500000)
        time_taken = time.time() - t_start
        self.assertGreaterEqual(time_taken, 0.4)
        self.assertGreaterEqual(micros(), 400)

    def test_ticks_us(self):

        self.assertLessEqual(ticks_us(), 5000)  # Should be very small at
        # the start

        time.sleep(0.20)

        ticks = ticks_us()

        self.assertTrue(150000 <= ticks <= 250000,
                        'ticks_us() is actually {}'.format(ticks))


if __name__ == '__main__':
    unittest.main()
