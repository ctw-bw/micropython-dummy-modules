import unittest

from pyb import Pin
from machine import ADC  # ADC is not inside pyb
from biorobotics import AnalogIn


class TestAnalogIn(unittest.TestCase):

    def test_adc(self):
        """Test pyb.ADC first"""

        p = Pin('A0')
        adc = ADC(p)
        self.assertEqual(0, adc.read_u16())
        Pin('A0').test_set_value(3000)
        self.assertEqual(3000, adc.read_u16())

    def test_analog_in(self):
        """Test fancy class"""
        a5 = AnalogIn('A5')
        self.assertEqual(0.0, a5.read())

        AnalogIn('A5').adc.test_set_value(9000)
        self.assertEqual(9000, a5.read_u16())
        self.assertAlmostEqual(0.137, a5.read(), delta=0.001)


if __name__ == '__main__':
    unittest.main()
