"""
Dummy module that does nothing.
Fakes part of the MicroPython modules.

Some classes are empty extensions of pyb classes to keep it DRY.
"""

from . import _mem
from pyb import Pin


# Create fake array objects that will just accept anything
mem8 = _mem.Mem()
mem32 = _mem.Mem()


class ADC:
    """Analog-to-Digital Converter

    Used to read an analog signal from a pin.
    Works only on pins with an ADC actually connected.

    Note that on the Nucleo the ADC class is only available under the
    machine package, not pyb. So we do the same in this dummy module.
    """

    # ADC get its values from the Pin object, so we don't need the _values
    # attribute here.

    def __init__(self, pin: Pin):
        """Constructor

        :param pin: ADC enabled pin
        """
        self.pin = pin

    def read_u16(self) -> int:
        """Read analog value"""

        # Take it directly from the pin itself
        return self.pin.value()

    def test_set_value(self, value: int):
        """Write a value to the pin"""
        self.pin.test_set_value(value)
